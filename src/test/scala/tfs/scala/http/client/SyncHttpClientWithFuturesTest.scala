package tfs.scala.http.client

import com.softwaremill.sttp._
import tfs.scala.http.client.util.{HttpClientTest, Measure}

import scala.concurrent._

class SyncHttpClientWithFuturesTest extends HttpClientTest {

  implicit val backend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()

  behavior of "SyncHttpClientWithFutures"

  it should s"send $iteration async requests" in Measure {
    Future.traverse((1 to iteration).toList) { _ =>
      Future {
        sendRequest()
      }
    }.futureValue
  }

  private def sendRequest(): Id[Response[String]] = {
    val request = sttp.get(uri"https://httpbin.org/delay/${timeout.toSeconds}")
    request.send()
  }
}